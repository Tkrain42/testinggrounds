// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DestroyableItem.generated.h"

UCLASS()
class TESTINGGROUNDS_API ADestroyableItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADestroyableItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMax = 5000, ClampMin = 1, UIMax = 5000, UIMin = 1))
	float MaxHealth = 100.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMax = 5000, ClampMin = 1, UIMax = 5000, UIMin = 1))
	float CurrentHealth = 100.0f;

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealthAsPercentage();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ToolTip = "Link meshes in order from healthy to destroyed.  Auto percentage will determine current mesh."))
	TArray<UStaticMeshComponent*> Meshes;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

	UFUNCTION(BlueprintPure, Category="Health")
	int MeshBasedOnHeath();

	UFUNCTION(BlueprintImplementableEvent, Category = "Display")
	void SetCorrectMesh();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
