// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

UCLASS()
class TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Places specified actor at random locations in the tile.  Adjust radius to fit location.
	UFUNCTION(BlueprintCallable, Category = "Construction")
	TArray<AActor*> PlaceActor(TSubclassOf<AActor> ActorToSpawn, int minSpawn = 1, int maxSpawn = 1, float Radius=500, bool Rotate=true, float ZOffset=0, float MinScale=1, float MaxScale=1);

	//Feed this with the Min and Max from a GetLocalActorBounds from the floor.
	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetBoxBoundaries(FVector MinBoxCorner, FVector MaxBoxCorner);

	UPROPERTY(BlueprintReadWrite, Category = "Construction")
		TArray<AActor*> Actors;

	UFUNCTION(BlueprintPure)
		float CalculateRequiredRadius(FVector MinCorner, FVector MaxCorner);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	//Returns True if location is clear to spawn
	bool IsLocationEmpty(FVector point, float radius);
	//Selects random locations until spherecast determines location is clear.  
	bool FindEmptyLocation(FVector & Location, float Radius);
	//min corner of box.
	FVector BoxMin = FVector(0, -2000, 0);
	//Max corner of box
	FVector BoxMax = FVector(4000, 2000, 0);
	//Spawns specified actor at location.  
	AActor* SpawnActorAt(TSubclassOf<AActor> &ActorToSpawn, const FVector &SpawnPoint, float Scaler);

	AActor* SpawnActorAt(TSubclassOf<AActor> &ActorToSpawn, const FVector &SpawnPoint, float Rotation, float Scaler);
};
