// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TestingGroundsAnimatorBase.generated.h"

/**
 * Wrapper class for animation blueprints, to allow variable conformity between the types.
 * The goal here is to have an animation Blueprint that simply calls Populate Variables for the major common stuff, and only has to worry about the character
 * specific actions here.
 */
UCLASS()
class TESTINGGROUNDS_API UTestingGroundsAnimatorBase : public UAnimInstance
{
	GENERATED_BODY()

//Note that these properties MUST be EditAnywhere in order to verify them in the test rig.
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float fSpeed = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float fDirection = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bJumpEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bJumping = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCrouching = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bShooting = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bAiming = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float fLookDirection = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDead = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bHit = false;

	UFUNCTION(BlueprintCallable)
	void PopulateVariables(class ATGCharacterBase* ATGCharacter);
};
