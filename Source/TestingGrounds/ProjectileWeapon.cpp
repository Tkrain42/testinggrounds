// (C) 2018 Brian K. Trotter

#include "ProjectileWeapon.h"
#include "Animation/AnimInstance.h"
#include "TestingGroundsProjectile.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"




// Sets default values
AProjectileWeapon::AProjectileWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	//// Create a gun mesh component
	//FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	////SetRootComponent(FP_Gun);
	//FP_Gun->SetOnlyOwnerSee(false);			// only the owning player will see this mesh
	//FP_Gun->bCastDynamicShadow = false;
	//FP_Gun->CastShadow = false;
	//// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	//

	//FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	//FP_MuzzleLocation->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	//FP_MuzzleLocation->SetupAttachment(FP_Gun);
	////FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

}

// Called when the game starts or when spawned
void AProjectileWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectileWeapon::AssignGun(USkeletalMeshComponent* Gun)
{
	FP_Gun = Gun;
}

void AProjectileWeapon::AssignMuzzle(USceneComponent * Muzzle)
{
	FP_MuzzleLocation = Muzzle;
}

void AProjectileWeapon::OnFire()
{
	if (!FP_MuzzleLocation)
	{
		UE_LOG(LogTemp, Warning, TEXT("Muzzle Not Set"))
		return;
	}
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			
			{
				const FRotator SpawnRotation = FP_MuzzleLocation->GetComponentRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = FP_MuzzleLocation->GetComponentLocation();

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

				// spawn the projectile at the muzzle
				auto Projectile = World->SpawnActor<ATestingGroundsProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
				if (Projectile)
				{
					Projectile->ResponsibleParty = ResponsibleParty;
					PlayShotSound();
					PlayFireAnimation();
					Projectile->MakeShotNoise();
				} else
				{
					return;
				}
			}
		}
	}

	
}

void AProjectileWeapon::PlayFireAnimation()
{
	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh

		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AProjectileWeapon::PlayShotSound()
{
	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
}

// Called every frame
void AProjectileWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileWeapon::SetResponsibleParty(APawn * Party)
{
	ResponsibleParty = Party;
}

