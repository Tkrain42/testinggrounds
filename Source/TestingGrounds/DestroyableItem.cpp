// (C) 2018 Brian K. Trotter

#include "DestroyableItem.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ADestroyableItem::ADestroyableItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADestroyableItem::BeginPlay()
{
	Super::BeginPlay();
	
}

float ADestroyableItem::GetHealthAsPercentage()
{
	if (MaxHealth <= 0)
	{
		return 0.0f;
	}
	return CurrentHealth / MaxHealth;
}

float ADestroyableItem::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	
	float ActualDamage = FMath::Clamp(DamageAmount, 0.0f, CurrentHealth);
	CurrentHealth -= ActualDamage;
	UE_LOG(LogTemp, Warning, TEXT("%s taking %f Damage, Health now %f"), *GetName(), ActualDamage, CurrentHealth)
	SetCorrectMesh();
	return ActualDamage;
}

int ADestroyableItem::MeshBasedOnHeath()
{
	int32 MeshCount = Meshes.Num();
	int32 MeshMax = FMath::Max(MeshCount - 1, 0);
	
	int32 result= FMath::Clamp(FMath::TruncToInt(GetHealthAsPercentage()*MeshCount), 0, MeshMax);
	UE_LOG(LogTemp, Warning, TEXT("MeshCount = %f, MeshMax=%f, Result=%f"), MeshCount, MeshMax, result);
	return result;
}



// Called every frame
void ADestroyableItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

