// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MeleeWeapon.generated.h"

UCLASS()
class TESTINGGROUNDS_API AMeleeWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMeleeWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UStaticMeshComponent* Weapon=nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float DamageBase = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float DamagePlusMinus = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	class UCapsuleComponent* DamageCapsule;

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void SetCapsule(UCapsuleComponent* Capsule);

	UPROPERTY(BlueprintReadWrite, Category = "Effects")
	class UParticleSystemComponent* ParticleSystem;

	UFUNCTION(BlueprintImplementableEvent)
	void PlayHitSound();

	APawn* ResponsibleParty = nullptr;

	AController* ResponsibleController = nullptr;
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure)
	float DamageRoll() const;

	UFUNCTION(BlueprintCallable, Category="Weapon")
	void SetResponsibleParty(APawn* Party);
	
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void SetDamageBase(float Damage);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void SetDamagePlusMinus(float Damage);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void SetResponsibleController(AController* Controller);

};
