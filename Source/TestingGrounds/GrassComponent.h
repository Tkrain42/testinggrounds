// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "GrassComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TESTINGGROUNDS_API UGrassComponent : public UHierarchicalInstancedStaticMeshComponent
{
	GENERATED_BODY()
	
	public:	
	// Sets default values for this component's properties
	UGrassComponent();

	UPROPERTY(EditDefaultsOnly, Category = "Spawning", meta = (ClampMax = 10000, ClampMin = 1, ToolTip = "The boundaries where the grass should be relative to the root of the tile.", UIMax = 10000, UIMin = 1))
	FBox SpawningExtents;
	UPROPERTY(EditDefaultsOnly, Category = Spawning)
	float ScaleMin = .5f;
	UPROPERTY(EditDefaultsOnly, Category = Spawning)
	float ScaleMax = 1.5f;
	UPROPERTY(EditDefaultsOnly, Category = Spawning)
	int SpawnMin=500;
	UPROPERTY(EditDefaultsOnly, Category = Spawning)
	int SpawnMax = 500;
	UPROPERTY(EditDefaultsOnly, Category = "Spawning", meta = (ToolTip = "Enter the longest edge of the grass mesh here  This will be used to calculate the radius for collission detection."))
	float LongestEdge = 200.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Spawning", meta = (ToolTip = "Max number of retries when searching for valid test location.  Increasing will improve chances of finding suitable locations.  Decreasing will result in better performance."))
		int TestRetries = 100;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category="Spawning")
	void SpawnGrass();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	

		//Returns True if location is clear to spawn
	bool IsLocationEmpty(FVector point, float radius);
	//Selects random locations until spherecast determines location is clear.  
	bool FindEmptyLocation(FVector & Location, FBox TryBox, float Radius);
	//min corner of box.
	bool ProperRadius() const;
	
};
