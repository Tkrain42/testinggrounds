// (C) 2018 Brian K. Trotter

#include "Tile.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"


// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
}

TArray<AActor*> ATile::PlaceActor(TSubclassOf<AActor> ActorToSpawn, int minSpawn, int maxSpawn, float Radius, bool Rotate, float ZOffset, float MinScale, float MaxScale)
{
	TArray<AActor*> ActorsSpawnedThisCall;
	ActorsSpawnedThisCall.Empty();
	if (!ActorToSpawn)
	{
		return ActorsSpawnedThisCall;
	}
	FVector SpawnPoint;
	int NumberToSpawn = FMath::RandRange(minSpawn, maxSpawn);
	AActor* NewActor=nullptr;
	for (int i = 0; i < NumberToSpawn; i++)
	{
		float RandomScale = FMath::FRandRange(MinScale, MaxScale);
		if (FindEmptyLocation(SpawnPoint, Radius*RandomScale))
		{
			SpawnPoint += FVector(0, 0, ZOffset);
			
			if (Rotate)
			{

				NewActor = (SpawnActorAt(ActorToSpawn, SpawnPoint, FMath::RandRange(0.0f, 360.0f), RandomScale));
			}
			else
			{
				NewActor = (SpawnActorAt(ActorToSpawn, SpawnPoint, RandomScale));
			}
			if (NewActor)
			{
				ActorsSpawnedThisCall.Add(NewActor);
			}
		}
	}
	return ActorsSpawnedThisCall;
}

void ATile::SetBoxBoundaries(FVector MinBoxCorner, FVector MaxBoxCorner)
{
	BoxMin = MinBoxCorner;
	BoxMin.X = 0;
	BoxMin.Z = 0;
	BoxMax = MaxBoxCorner;
	BoxMax.X = FMath::Abs(MaxBoxCorner.X - MinBoxCorner.X);
	BoxMax.Z = 0;
}

AActor* ATile::SpawnActorAt(TSubclassOf<AActor> &ActorToSpawn, const FVector &SpawnPoint, float Scaler)
{
	return SpawnActorAt(ActorToSpawn, SpawnPoint, 0.0f, Scaler);
}

AActor* ATile::SpawnActorAt(TSubclassOf<AActor>& ActorToSpawn, const FVector & SpawnPoint, float Rotation, float Scaler)
{
	auto NewActor = GetWorld()->SpawnActor<AActor>(ActorToSpawn);
	if (!NewActor)
	{
		return nullptr;
	}
	auto Scale = NewActor->GetActorScale3D()* Scaler;
	NewActor->SetActorScale3D(Scale);

	NewActor->SetActorRelativeLocation(SpawnPoint);
	NewActor->SetActorRelativeRotation(FRotator(0,Rotation,0));
	NewActor->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	Actors.Add(NewActor);
	return NewActor;
}



bool ATile::IsLocationEmpty(FVector point, float radius)
{
	FHitResult Hit;
	FVector GlobalPoint = ActorToWorld().TransformPosition(point);
	bool result =GetWorld()->SweepSingleByChannel(Hit, GlobalPoint, GlobalPoint, FQuat::Identity, ECC_GameTraceChannel2, FCollisionShape::MakeSphere(radius));
	//FColor SweepColor = result ? FColor::Red : FColor::Green;
	//DrawDebugSphere(GetWorld(), GlobalPoint, radius, 5, SweepColor, true);
	return !result;
}

bool ATile::FindEmptyLocation(FVector & Location, float Radius)
{
	FBox Box = FBox(BoxMin, BoxMax);
	FVector TestLocation;
	for (int i = 0; i < 100; i++)
	{
		Location = FMath::RandPointInBox(Box);
		if (IsLocationEmpty(Location, Radius))
		{
			return true;
		}
	}
	return false;
}

float ATile::CalculateRequiredRadius(FVector MinCorner, FVector MaxCorner)
{
	FVector MinC = MinCorner;
	FVector MaxC = MaxCorner;
	MinC.X = 0;
	MinC.Z = 0;
	MaxC.X = FMath::Abs(MinCorner.X) + FMath::Abs(MaxCorner.X);
	MaxC.Z = 0;
	return FVector::Distance(MinC, MaxC)/2.0f;
	
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

