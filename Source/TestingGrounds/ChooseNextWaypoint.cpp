// (C) 2018 Brian K. Trotter

#include "ChooseNextWaypoint.h"
#include "TP_ThirdPerson/TP_ThirdPersonCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "PatrolRoute.h"
#include "Classes/AIController.h"

EBTNodeResult::Type UChooseNextWaypoint::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	auto MyController = OwnerComp.GetAIOwner();
	auto MyCharacter = MyController->GetPawn()->FindComponentByClass<UPatrolRoute>();
	
	auto Blackboard = OwnerComp.GetBlackboardComponent();
	if (MyCharacter)
	{
		
		
		int CurrentIndex = Blackboard->GetValueAsInt(Index.SelectedKeyName);
		AActor* NewWaypoint = MyCharacter->GetWaypointAt(CurrentIndex);
		Blackboard->SetValueAsObject(Waypoint.SelectedKeyName, NewWaypoint);
		//UE_LOG(LogTemp, Warning, TEXT("%s advancing to waypoint %i of %i"), *MyCharacter->GetName(), CurrentIndex, MyCharacter->GetWaypointCount());
		CurrentIndex++;
		if (MyCharacter->GetWaypointCount() > 0)
		{
			CurrentIndex = CurrentIndex % MyCharacter->GetWaypointCount();
		}
		Blackboard->SetValueAsInt(Index.SelectedKeyName, CurrentIndex);
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("Unable to locate owned Third Person Character."))
			Blackboard->SetValueAsObject(Waypoint.SelectedKeyName,MyController->GetPawn());
	}
	return EBTNodeResult::Type();
}
