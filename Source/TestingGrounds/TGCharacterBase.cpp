// (C) 2018 Brian K. Trotter

#include "TGCharacterBase.h"
#include "Components/CapsuleComponent.h"

#define LOCTEXT_NAMESPACE "TestingGrounds" 
// Sets default values
ATGCharacterBase::ATGCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATGCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
	auto Controller = GetController();
	if (!Controller)
	{
		SpawnDefaultController();
	}
	Jump();
	
}

float ATGCharacterBase::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (!IsAlive())
	{
		return 0.0f;
	}
	float ActualDamage = FMath::Min(DamageAmount, CurrentHealth);
	CurrentHealth -= ActualDamage;
	if (!IsAlive())
	{
		auto OtherTGCharacter = Cast<ATGCharacterBase>(DamageCauser);
		if (OtherTGCharacter)
		{
			OtherTGCharacter->AddExperience(GetLevel() * 25);
		}
		OnDeath();
	} else
	{
		LastTimeHit = 0.25f;
		OnCharacterHasTakenDamage(ActualDamage);
	}
	return ActualDamage;
}

void ATGCharacterBase::ResetCapsuleComponent(UCapsuleComponent * Capsule)
{
	Capsule->SetCanEverAffectNavigation(false);
}

// Called every frame
void ATGCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Firing = FMath::Max(Firing - DeltaTime, 0.0f);
	LastTimeHit = FMath::Max(LastTimeHit - DeltaTime, 0.0f);
}

// Called to bind functionality to input
void ATGCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ATGCharacterBase::IsAlive()
{
	return (CurrentHealth > 0.0f);
}

bool ATGCharacterBase::ReactToHit()
{
	return (LastTimeHit > 0.0f);
}

float ATGCharacterBase::GetHealthAsPercentage()
{
	return CurrentHealth / MaxHealth;
}

bool ATGCharacterBase::IsInAttack()
{
	return Firing > 0.0;
}

FLevelAttributes ATGCharacterBase::GetLevelAttributes()
{
	return Attributes;
}

float ATGCharacterBase::MaxHealthBasedOnLevel()
{
	return Attributes.MaxHealthBasedOnLevel();
}

float ATGCharacterBase::SpeedBoostBasedOnLevel()
{
	return Attributes.SpeedBoostBasedOnLevel();
}

float ATGCharacterBase::AttackMultiplier()
{
	return Attributes.AttackMultiplier();
}

float ATGCharacterBase::MeleeDefenseMultiplier()
{
	return Attributes.MeleeDefenseMultiplier();
}

float ATGCharacterBase::RangeDefenseMultiplier()
{
	return Attributes.RangeDefenseMultiplier();
}

float ATGCharacterBase::MagicDefenseMultiplier()
{
	return Attributes.MagicDefenseMultiplier();
}

int32 ATGCharacterBase::ExperienceToNextLevel()
{
	return Attributes.ExperienceToNextLevel();
}

FString ATGCharacterBase::GetCName()
{
	return Attributes.CName;
}

int32 ATGCharacterBase::GetExperience()
{
	return Attributes.Experience;
}

int32 ATGCharacterBase::GetLevel()
{
	return Attributes.Level;
}

int32 ATGCharacterBase::GetHealthModifier()
{
	return Attributes.HealthModifier;
}

int32 ATGCharacterBase::GetMeleeDefense()
{
	return Attributes.MeleeDefense;
}

int32 ATGCharacterBase::GetRangeDefense()
{
	return Attributes.RangeDefense;
}

int32 ATGCharacterBase::GetMagicDefense()
{
	return Attributes.MagicDefense;
}

int32 ATGCharacterBase::GetOffenseBoost()
{
	return Attributes.OffenseBoost;
}

int32 ATGCharacterBase::GetSpeedBoost()
{
	return Attributes.SpeedBoost;
}

int32 ATGCharacterBase::GetUnspentPoints()
{
	return Attributes.UnspentPoints;
}

FString ATGCharacterBase::SetCName(FString NewName)
{
	Attributes.CName = NewName;
	return NewName;
}

int32 ATGCharacterBase::SetExperience(int32 NewExperience)
{
	Attributes.Experience = FMath::Max(0,NewExperience);
	while (Attributes.Experience >= ExperienceToNextLevel())
	{
		Attributes.Level++;
		Attributes.Experience = FMath::Max(0, Attributes.Experience - ExperienceToNextLevel());
		OnLevelledUp();
	}
	return Attributes.Experience;
}

int32 ATGCharacterBase::SetLevel(int32 NewLevel)
{
	Attributes.Level = FMath::Max(1, NewLevel);
	return Attributes.Level;
}

int32 ATGCharacterBase::SetHealthModifier(int32 NewHealthModifier)
{
	Attributes.HealthModifier = FMath::Max(0, NewHealthModifier);
	return Attributes.HealthModifier;
}

int32 ATGCharacterBase::SetMeleeDefense(int32 NewMeleeDefense)
{
	Attributes.MeleeDefense = FMath::Max(0, NewMeleeDefense);
	return NewMeleeDefense;
}

int32 ATGCharacterBase::SetRangeDefense(int32 NewRangeDefense)
{
	Attributes.RangeDefense = FMath::Max(0, NewRangeDefense);
	return NewRangeDefense;
}

int32 ATGCharacterBase::SetMagicDefense(int32 NewMagicDefense)
{
	Attributes.MagicDefense = FMath::Max(0,NewMagicDefense);
	return Attributes.MagicDefense;
}

int32 ATGCharacterBase::SetOffenseBoost(int32 NewOffenseBoost)
{
	Attributes.OffenseBoost = FMath::Max(0, NewOffenseBoost);
	return Attributes.OffenseBoost;
}

int32 ATGCharacterBase::SetSpeedBoost(int32 NewSpeedBoost)
{
	Attributes.SpeedBoost = FMath::Max(0, NewSpeedBoost);
	return Attributes.SpeedBoost;
}

int32 ATGCharacterBase::SetUnspentPoints(int32 NewUnspentPoints)
{
	Attributes.UnspentPoints = FMath::Max(0, NewUnspentPoints);
	return Attributes.UnspentPoints;
}

int32 ATGCharacterBase::AddExperience(int32 ExperienceToAdd)
{
	auto NewExperience = Attributes.Experience + ExperienceToAdd;
	return SetExperience(NewExperience);
}

FText ATGCharacterBase::ExperienceText()
{
	FFormatNamedArguments Arguments;
	Arguments.Add(TEXT("Exp"), FText::AsNumber(Attributes.Experience));
	Arguments.Add(TEXT("Need"), FText::AsNumber(Attributes.ExperienceToNextLevel()));
	return FText::Format(LOCTEXT("ExperienceText", "Exp: {Exp}/{Need}"), Arguments );
}

FString ATGCharacterBase::HealthText()
{
	return FString();
}

FString ATGCharacterBase::HealthModText()
{
	return FString();
}

FString ATGCharacterBase::MeleeModText()
{
	return FString();
}

FString ATGCharacterBase::RangeModText()
{
	return FString();
}

FString ATGCharacterBase::MagicModText()
{
	return FString();
}

FString ATGCharacterBase::OffenseBoostText()
{
	return FString();
}

FString ATGCharacterBase::SpeedBoostText()
{
	return FString();
}

FString ATGCharacterBase::UnspentPointsText()
{
	return FString();
}

float FLevelAttributes::MaxHealthBasedOnLevel()
{
	float TotalHealthModifier = Level + (float)HealthModifier/2.0f;

	return 100.0f*TotalHealthModifier;
}

float FLevelAttributes::SpeedBoostBasedOnLevel()
{
	float SpeedModifier = Level + (float)SpeedBoost / 2.0f;

	return 1.0 + SpeedModifier / 20.0f;
}

float FLevelAttributes::AttackMultiplier()
{
	float Multiplier = Level + (float)OffenseBoost / 2.0f;
	return Multiplier + 1.0f;
}

float FLevelAttributes::MeleeDefenseMultiplier()
{
	float Multiplier = Level + (float)MeleeDefense / 2.0f;
	return Multiplier + 1.0f;
}

float FLevelAttributes::RangeDefenseMultiplier()
{
	float Multiplier = Level + (float)RangeDefense / 2.0f;
	return Multiplier + 1.0f;
}

float FLevelAttributes::MagicDefenseMultiplier()
{
	float Multiplier = Level + (float)MagicDefense / 2.0f;
	return Multiplier + 1.0f;
}

int32 FLevelAttributes::ExperienceToNextLevel()
{
	float Multiplier = Level+(float)Level / 2.0f;
	return 100 * Multiplier;
}

#undef LOCTEXT_NAMESPACE 
