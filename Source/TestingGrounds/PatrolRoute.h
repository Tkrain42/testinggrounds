// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PatrolRoute.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTINGGROUNDS_API UPatrolRoute : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPatrolRoute();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Patrol", meta = (ToolTip = "List of patrol waypoints.  Usually Targets."))
	TArray<AActor*> PatrolWaypoints;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		//Index safe retrieval of waypoint, will always modulo index to insure it grabs a waypoint within range.
	UFUNCTION(BlueprintPure, Category = "Patrol")
		AActor*	GetWaypointAt(int32 Index) const;

	//The number of waypoints assigned to this guard.
	UFUNCTION(BlueprintPure, Category = "Patrol")
		int32 GetWaypointCount() const;
		
	UFUNCTION(BlueprintCallable, Category = "Setup")
		void AddWayPoint(AActor* Waypoint);
};
