// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestingGroundsProjectile.generated.h"

UCLASS(config=Game)
class ATestingGroundsProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

public:
	ATestingGroundsProjectile();

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(BlueprintReadWrite)
	APawn* ResponsibleParty;

	UPROPERTY(BlueprintReadWrite)
		AController* ResponsibleController;
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Behavior", meta = (ToolTip = "Used to make a noise when firing"))
	void MakeShotNoise();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMax = 100, ClampMin = 0))
		float DamageBase = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMax = 100, ClampMin = 0))
		float DamagePlusMinus = 3.0f;

	UFUNCTION(BlueprintPure)
	float DamageRoll() const;

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
};

