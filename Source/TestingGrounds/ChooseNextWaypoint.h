// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "ChooseNextWaypoint.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API UChooseNextWaypoint : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
protected:
	//Current waypoint index
	UPROPERTY(EditAnywhere, Category = "Blueprint")
	struct FBlackboardKeySelector Index;
	//
	UPROPERTY(EditAnywhere, Category = "Blueprint")
	struct FBlackboardKeySelector Waypoint;
};
