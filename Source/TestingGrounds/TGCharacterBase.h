// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TGCharacterBase.generated.h"

USTRUCT(BlueprintType, Category="Attributes")
struct FLevelAttributes
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelAttributes")
	FString CName = "Generic Character";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelAttributes", meta = (ToolTip = "Accumulates as you progress through levels and defeat enemies."))
	int32 Experience = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelAttributes", meta = (ToolTip = "Used to generate automatic modifiers to subsequent attributes"))
	int32 Level = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelAttributes", meta = (ToolTip = "In levels.  Added to Level before calculating character health."))
	int32 HealthModifier = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelAttributes", meta = (ToolTip = "In levels.  Added to Level before calculating Melee Defense"))
	int32 MeleeDefense = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LevelAttributes", meta = (ToolTip = "In levels.  Added to Level before calculating Ranged Defense"))
	int32 RangeDefense = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LevelAttributes", meta = (ToolTip = "In levels.  Added to Level before calculating Magic Defense"))
	int32 MagicDefense = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LevelAttributes", meta = (ToolTip = "In levels.  Added to Level before calculating Offensive boost."))
	int32 OffenseBoost = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LevelAttributes")
	int32 SpeedBoost = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LevelAttributes", meta = (ToolTip = "Unspent points.  Earned by levelling or from special items."))
	int32 UnspentPoints = 0;

	float MaxHealthBasedOnLevel();

	float SpeedBoostBasedOnLevel();

	float AttackMultiplier();

	float MeleeDefenseMultiplier();

	float RangeDefenseMultiplier();

	float MagicDefenseMultiplier();

	int32 ExperienceToNextLevel();

	

};

UCLASS()
class TESTINGGROUNDS_API ATGCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATGCharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHealth = 100.0f;

	UPROPERTY(BlueprintReadWrite, Category = "Health")
		float CurrentHealth=100.0f;

	UPROPERTY(BlueprintReadWrite, Category = "Firing")
		float Firing = 0.0f;

	UPROPERTY(BlueprintReadWrite, Category = "Firing")
		bool IsAiming = false;

	UPROPERTY(BlueprintReadWrite, Category = "Patrol")
		bool LookAround = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Firing")
		float AttackRange = 300.0f;

	UFUNCTION(BlueprintImplementableEvent, meta = (ToolTip = "Called when TakeDamage determines that you're dead."))
	void OnDeath();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	FString DisplayName = "Generic Character";

	float LastTimeHit = 0.0f;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Behavior")
	class UBehaviorTree* BehaviorTree = nullptr;

	UFUNCTION(BlueprintCallable, Category = "Kludges")
		void ResetCapsuleComponent(UCapsuleComponent* Capsule);

	UPROPERTY(BlueprintReadwrite, Category = "LevelAttributes")
	FLevelAttributes Attributes;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void InitiateAttack();

	UFUNCTION(BlueprintPure)
	bool IsAlive();
	
	UFUNCTION(BlueprintPure)
	bool ReactToHit();

	UFUNCTION(BlueprintPure)
	float GetHealthAsPercentage();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetCharacterSpeed(float NewSpeed);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	float GetCharacterSpeed();

	//Event called by AI Controller when the enemy is spotted in perception.  This would be a good place to make an "Aha" sound or fire off a particle system.
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ReactToSeeingEnemy();
	//Event called by AI Controller when the character has lost sight of the enemy.   This would be a good place to make an "Aha" sound or fire off a particle system.
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ReactToLosingEnemy();
	
	UFUNCTION(BlueprintImplementableEvent, meta = (Tooltip = "Called when you're damaged, gives you a chance to grunt or play an effect."))
	void OnCharacterHasTakenDamage(float Amount);
	
	UFUNCTION(BlueprintPure)
		bool IsInAttack();

	UFUNCTION(BlueprintPure)
	FLevelAttributes GetLevelAttributes();
	
	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	float MaxHealthBasedOnLevel();
	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	float SpeedBoostBasedOnLevel();
	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	float AttackMultiplier();
	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	float MeleeDefenseMultiplier();
	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	float RangeDefenseMultiplier();
	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	float MagicDefenseMultiplier();
	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 ExperienceToNextLevel();

	UFUNCTION(BlueprintImplementableEvent, Category = "LevelAttributes")
	void OnLevelledUp();

	bool CharacterIsAiming()
	{
		return IsAiming;
	}

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	FString GetCName();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetExperience();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetLevel();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetHealthModifier();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetMeleeDefense();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetRangeDefense();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetMagicDefense();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetOffenseBoost();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetSpeedBoost();

	UFUNCTION(BlueprintPure, Category = "LevelAttributes")
	int32 GetUnspentPoints();
	

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	FString SetCName(FString NewName);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetExperience(int32 NewExperience);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetLevel(int32 NewLevel);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetHealthModifier(int32 NewHealthModifier);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetMeleeDefense(int32 NewMeleeDefense);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetRangeDefense(int32 NewRangeDefense);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetMagicDefense(int32 NewMagicDefense);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetOffenseBoost(int32 NewOffenseBoost);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetSpeedBoost(int32 NewSpeedBoost);

	UFUNCTION(BlueprintCallable, Category="LevelAttributes")
	int32 SetUnspentPoints(int32 NewUnspentPoints);

	UFUNCTION(BlueprintCallable, Category = "LevelAttributes")
	int32 AddExperience(int32 ExperienceToAdd);

	UFUNCTION(BlueprintPure, Category = "UI")
		FText ExperienceText();

	UFUNCTION(BlueprintPure, Category = "UI")
		FString HealthText();
	
	UFUNCTION(BlueprintPure, Category = "UI")
		FString HealthModText();

	UFUNCTION(BlueprintPure, Category = "UI")
		FString MeleeModText();

	UFUNCTION(BlueprintPure, Category = "UI")
		FString RangeModText();

	UFUNCTION(BlueprintPure, Category = "UI")
		FString MagicModText();

	UFUNCTION(BlueprintPure, Category = "UI")
		FString OffenseBoostText();

	UFUNCTION(BlueprintPure, Category = "UI")
		FString SpeedBoostText();

	UFUNCTION(BlueprintPure, Category = "UI")
		FString UnspentPointsText();
};
