// (C) 2018 Brian K. Trotter

#include "TestingGroundsAnimatorBase.h"
#include "TGCharacterBase.h"
#include "GameFramework/PawnMovementComponent.h"

void UTestingGroundsAnimatorBase::PopulateVariables(ATGCharacterBase* ATGCharacter)
{
	if (ATGCharacter)
	{
		bAiming = ATGCharacter->CharacterIsAiming();
		bDead = !ATGCharacter->IsAlive();
		bShooting = ATGCharacter->IsInAttack();
		bHit = ATGCharacter->ReactToHit();
		auto velocity = ATGCharacter->GetVelocity();
		fSpeed = velocity.Size();
		auto rotation = ATGCharacter->GetActorRotation();
		fDirection = CalculateDirection(velocity, rotation);
		auto movement = ATGCharacter->GetMovementComponent();
		bJumping = movement->IsFalling();
	}
}
