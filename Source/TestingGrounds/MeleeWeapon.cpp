// (C) 2018 Brian K. Trotter

#include "MeleeWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"
#include "Components/CapsuleComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "TGCharacterBase.h"


// Sets default values
AMeleeWeapon::AMeleeWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMeleeWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMeleeWeapon::SetCapsule(UCapsuleComponent * Capsule)
{
	DamageCapsule = Capsule;
	if (DamageCapsule)
	{
		DamageCapsule->OnComponentBeginOverlap.AddDynamic(this, &AMeleeWeapon::OnOverlapBegin);
	}
}

void AMeleeWeapon::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor == Cast<AActor>(ResponsibleParty))
	{
		return;
	}
	if (!Cast<ATGCharacterBase>(ResponsibleParty)->IsInAttack())
	{
		return;
	}

	if (OtherActor != NULL)
	{
		
		auto DamageDelivered = UGameplayStatics::ApplyDamage(OtherActor, DamageRoll(), ResponsibleController, Cast<AActor>(ResponsibleParty), UDamageType::StaticClass());
		if (DamageDelivered <1.0f)
		{
			PlayHitSound();
		}
		if (ParticleSystem != NULL)
		{
			ParticleSystem->Activate(true);
		}
	}
}

// Called every frame
void AMeleeWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float AMeleeWeapon::DamageRoll() const
{
	return FMath::Max(0.0f, DamageBase+FMath::FRandRange(-DamagePlusMinus,DamagePlusMinus));
}

void AMeleeWeapon::SetResponsibleParty(APawn * Party)
{
	ResponsibleParty = Party;
}

void AMeleeWeapon::SetDamageBase(float Damage)
{
	DamageBase = FMath::Max(0.0f, Damage);
}

void AMeleeWeapon::SetDamagePlusMinus(float Damage)
{
	DamagePlusMinus = FMath::Max(0.0f, Damage);
}

void AMeleeWeapon::SetResponsibleController(AController * Controller)
{
	ResponsibleController = Controller;
}

