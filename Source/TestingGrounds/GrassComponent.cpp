// (C) 2018 Brian K. Trotter

#include "GrassComponent.h"


// Sets default values for this component's properties
UGrassComponent::UGrassComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrassComponent::BeginPlay()
{
	Super::BeginPlay();

	//SpawnGrass();
}

void UGrassComponent::SpawnGrass() 
{
	
	float minX = FMath::Min(SpawningExtents.Min.X, SpawningExtents.Max.X);
	float maxX = FMath::Max(SpawningExtents.Min.X, SpawningExtents.Max.X);
	float minY = FMath::Min(SpawningExtents.Min.Y, SpawningExtents.Max.Y);
	float maxY = FMath::Max(SpawningExtents.Min.Y, SpawningExtents.Max.Y);
	
	int32 step = (maxX - minX) / SpawnMin;

	int SpawnCount = FMath::RandRange(SpawnMin, SpawnMax);
	for (size_t i = minX; i < maxX; i+=step)
		for (int32 j = minY;j<maxY;j+=step)

	{
		FVector Location;
		float Scaler = FMath::FRandRange(ScaleMin, ScaleMax);
		FBox TryBox=FBox(FVector(i,j,0), FVector(i+LongestEdge/2, j+LongestEdge/2,0));
		if (FindEmptyLocation(Location, TryBox, LongestEdge/2))
		{
			float Rotation = FMath::FRandRange(0, 360);
			FRotator Rotator = FRotator(0, 0, Rotation);
			
			FVector Scale = FVector(Scaler, Scaler, Scaler);
			AddInstance(FTransform(Rotator, Location, Scale));
		}
	}
}

bool UGrassComponent::IsLocationEmpty(FVector point, float radius)
{
	FHitResult Hit;
	FVector GlobalPoint = GetOwner()->ActorToWorld().TransformPosition(point);
	bool result =GetWorld()->SweepSingleByChannel(Hit, GlobalPoint, GlobalPoint, FQuat::Identity, ECC_GameTraceChannel2, FCollisionShape::MakeSphere(radius));
	//FColor SweepColor = result ? FColor::Red : FColor::Green;
	//DrawDebugSphere(GetWorld(), GlobalPoint, radius, 5, SweepColor, true);
	return !result;
}

bool UGrassComponent::FindEmptyLocation(FVector & Location, FBox TryBox,  float Radius)
{
	
	FVector TestLocation;
	for (int i = 0; i < TestRetries; i++)
	{
		Location = FMath::RandPointInBox(TryBox);
		if (IsLocationEmpty(Location, Radius))
		{
			return true;
		}
	}
	return false;
}

bool UGrassComponent::ProperRadius() const
{
	float ASquared = LongestEdge * LongestEdge* .4f;
	return FMath::Sqrt(ASquared + ASquared) / 2.0f;
}

// Called every frame
void UGrassComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}



